Perlembed Playgroud
===================

This repository showcases the usage of embedded Perl in a C program.

Build
-----

This project uses meson to figure out all necessary options to compile the
program. Since there is no pkg-config module for perl, meson uses calls to
the Perl module `ExtUtils::Embed` for the correrct parameters.

Dependencies:

* Perl
* meson

To build from command line use these commands:

```sh
mkdir build
cd build
meson ..
ninja
```

References
----------

Documentation for programming with the C-API of Perl:

* https://perldoc.perl.org/perlembed
* https://perldoc.perl.org/perlcall
* https://perldoc.perl.org/perlguts
* https://perldoc.perl.org/perlapi
* [Doug MacEachern – Best of Both Worlds: Embedding Perl in C](https://www.foo.be/docs/tpj/issues/vol1_4/tpj0104-0009.html)

Getting it to build:

* [ExtUtils::Embed POD](https://metacpan.org/pod/ExtUtils::Embed)
* [Meson Manual](https://mesonbuild.com/Manual.html)
* [Meson Reference](https://mesonbuild.com/Reference-manual.html)
