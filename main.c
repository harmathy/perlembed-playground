/* main.c
 *
 * Copyright 2020 Max Harmathy
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <EXTERN.h>
#include <perl.h>

static PerlInterpreter *my_perl;

static long perl_int_power(int a, int b) {
    dSP;
    ENTER;
    SAVETMPS;
    PUSHMARK(SP);
    XPUSHs(sv_2mortal(newSViv(a)));
    XPUSHs(sv_2mortal(newSViv(b)));
    PUTBACK;
    call_pv("expo", G_SCALAR);
    SPAGAIN;
    long result = POPi;
    PUTBACK;
    FREETMPS;
    LEAVE;
    return result;
}

static char *perl_str_power(char *a, int b) {
    size_t input_length = strlen(a);

    dSP;
    ENTER;
    SAVETMPS;
    PUSHMARK(SP);
    XPUSHs(sv_2mortal(newSVpv(a, input_length)));
    XPUSHs(sv_2mortal(newSViv(b)));
    PUTBACK;
    call_pv("explo", G_SCALAR);
    SPAGAIN;

    char *value = POPp;

    size_t value_length = strlen(value);
    size_t result_length = value_length + 1;
    char *result = malloc((result_length) * sizeof(char));
    if (result != NULL) {
        strncpy(result, value, result_length);
    }

    PUTBACK;
    FREETMPS;
    LEAVE;
    return result;
}

int main(int argc, char **argv, char **env) {
    char *embedding[] = {"", "-e", "0", NULL};

    PERL_SYS_INIT3(&argc, &argv, &env);
    my_perl = perl_alloc();
    perl_construct(my_perl);

    perl_parse(my_perl, NULL, 3, embedding, NULL);
    PL_exit_flags |= PERL_EXIT_DESTRUCT_END;
    perl_run(my_perl);

    eval_pv("sub expo { my ($a, $b) = @_; return $a ** $b; }", TRUE);

    int a = 3;
    int b = 4;

    long result = perl_int_power(a, b);

    printf("perl_int_power(%d, %d) = %ld\n\n", a, b, result);

    eval_pv("sub explo { my ($a, $b) = @_; return $a x $b; }", TRUE);

    char *c = "a";

    char *str_result = perl_str_power(c, b);

    printf("perl_str_power(\"%s\", %d) = \"%s\"\n\n", c, b, str_result);

    perl_destruct(my_perl);
    perl_free(my_perl);
    PERL_SYS_TERM();
}
